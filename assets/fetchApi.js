// funktsoonid, mis suhtlevad java serveriga
async function fetchCompanies() {
    let result = await fetch (`${API_URL}/companies`);// see url on seootud config.js koodis määratud pathiga
    let companies= await result.json();
    return companies;
}
async function deleteCompany(companyId){
    await fetch(`${API_URL}/companies/${companyId}`, {method:'DELETE'});
}
async function saveCompany(company){
    await fetch(`${API_URL}/companies`, {
        method:'POST', 
     headers:{
         'Content-Type': 'application/json'
    }, 
     body: JSON.stringify(company)
    });
}

async function fetchCompany(companyId) {
    let result = await fetch (`${API_URL}/companies/${companyId}`);// see url on seootud config.js koodis määratud pathiga
    let company= await result.json();
    return company;
}