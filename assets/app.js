let currentCompanyId=0;

// Initialization after the HTML document has been loaded...
// siia tulevad kõik kasulikud funktsioonid, see määrab kuidas veebileht välja näeb
window.addEventListener('DOMContentLoaded', () => {
    // event handler mille event on kui vontent on laetud

    // Function calls for initial page loading activities...
    doLoadCompanies();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadCompanies() {
    let companies=await fetchCompanies();
    console.log('Loading companies...');
    console.log(companies);

    let companiesDiv = document.querySelector('#companiesList')

    let htmlContent ='';

    for(let company of companies){
        htmlContent = htmlContent + /*html*/`
        <div id=container>
        <div class="detail-title"> Ettevõtte nimi: </div>
        <div class="detail-value">${company.name}</div>
        <div class="detail-title"> Ettevõtte logo: </div>
        <div class="detail-value"><img src="${company.logo}" width="150"></div>    
        <div>
        <button class= "button-red" onclick ="doDeleteCompany(${company.id})">kustuta</button>
        <button onclick="doOpenPopUp(${company.id})">MUUDA</button>
        </div>    

        </div>

        
        `
        // '' + company.name;

    }

    companiesDiv.innerHTML=htmlContent;
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {

}
async function doDeleteCompany(companyId){
    if(confirm ('Do you want to delete this company?'))
    {await deleteCompany(companyId);
    doLoadCompanies();}

}

async function displayCompanyEditPopup(id) {
    await openPopup(POPUP_BLANK_300_300, 'companyEditFormTemplate');//viitab popup-ile popup.js-is ja templatile html-is
}
async function doOpenPopUp (companyId){
    await openPopup(POPUP_BLANK_300_300, "companyEditFormTemplate");
    if (companyId > 0){
        console.log("changeing company");
        let company= await fetchCompany(companyId);
        console.log(company);
        let nameTextBox = document.querySelector('#name');
        let nameLogoBox = document.querySelector('#logo');

        nameTextBox.value=company.name;
        nameLogoBox.value=company.logo;
        currentCompanyId=company.id;
    }else{
        console.log("adding company");
        currentCompanyId=0;
        

    }


}
async function doSaveCompany(){
    console.log("saving company");
    let nameTextBox = document.querySelector('#name');
    let nameLogoBox = document.querySelector('#logo');
    let companyName = nameTextBox.value;
    let companyLogo = nameLogoBox.value;
    let company={
        id: currentCompanyId,
        name:companyName,
        logo:companyLogo
    };
    console.log(company);
    await saveCompany(company);
    await doLoadCompanies();
    await closePopup();
    
}

// result = num1*num2
// resultContainer.textContent=result;
